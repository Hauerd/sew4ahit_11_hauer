﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Mini_CAD
{
    class Line :Shapes
    {
        int x2;
        int y2;

        public Line(int x1, int x2, int y1, int y2, Color color, int lineWidth):base(x1, y1, color, lineWidth)
        {
            this.x2 = x2;
            this.y2 = y2;
        }

        public override void Draw(Graphics g)
        { 
            g.DrawLine(new Pen(color,lineWidth),new Point(x1,y1),new Point (x2,y2));

            if (selected)
            {
                g.DrawRectangle(Pens.Red, x1 - 5, y1 - 5, 10, 10);
                g.DrawRectangle(Pens.Red, x2 - 5, y2 - 5, 10, 10);

                selected = false;
            }
        }

        public override bool AmISelected(int x, int y)
        {
            
            if (x >= x1-5 && x <= x2+5 && y >= y1-5 && y <= y2+5)
            {
                selected = true;
                return selected;
            }
            else
            {
                selected = false;
                return selected;
            }
        }

        public Point GetStartPoint()
        {
            return new Point(x1, y1);
        }

        public Point GetEndPoint()
        {
            return new Point(x2,y2);
        }

        public override string GetSaveString()
        {
            return "Line;" + "x1=" + x1 + ";y1=" + y1 + ";x2=" + x2 + ";y2=" + y2 + ";colorR=" + color.R + ";colorG=" + color.G + ";colorB=" + color.B + ";penWidth=" + lineWidth;
        }
    }
}
