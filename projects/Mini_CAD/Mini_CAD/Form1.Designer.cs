﻿namespace Mini_CAD
{
    partial class Mini_CAD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mini_CAD));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Line_Pannel = new System.Windows.Forms.Panel();
            this.lt_y2 = new System.Windows.Forms.MaskedTextBox();
            this.lt_y1 = new System.Windows.Forms.MaskedTextBox();
            this.lt_x2 = new System.Windows.Forms.MaskedTextBox();
            this.lt_x1 = new System.Windows.Forms.MaskedTextBox();
            this.l_y2 = new System.Windows.Forms.Label();
            this.l_x2 = new System.Windows.Forms.Label();
            this.l_y1 = new System.Windows.Forms.Label();
            this.l_x1 = new System.Windows.Forms.Label();
            this.Linie = new System.Windows.Forms.Label();
            this.draw_Pannel = new System.Windows.Forms.Panel();
            this.draw = new System.Windows.Forms.Button();
            this.Rect_Pannel = new System.Windows.Forms.Panel();
            this.rt_height = new System.Windows.Forms.MaskedTextBox();
            this.rt_width = new System.Windows.Forms.MaskedTextBox();
            this.rt_y2 = new System.Windows.Forms.MaskedTextBox();
            this.rt_y1 = new System.Windows.Forms.MaskedTextBox();
            this.rt_x2 = new System.Windows.Forms.MaskedTextBox();
            this.rt_x1 = new System.Windows.Forms.MaskedTextBox();
            this.r_height = new System.Windows.Forms.Label();
            this.r_width = new System.Windows.Forms.Label();
            this.r_y2 = new System.Windows.Forms.Label();
            this.r_y1 = new System.Windows.Forms.Label();
            this.r_x2 = new System.Windows.Forms.Label();
            this.r_x1 = new System.Windows.Forms.Label();
            this.Rectangle = new System.Windows.Forms.Label();
            this.Circle_Pannel = new System.Windows.Forms.Panel();
            this.ct_radius = new System.Windows.Forms.MaskedTextBox();
            this.ct_y1 = new System.Windows.Forms.MaskedTextBox();
            this.ct_x1 = new System.Windows.Forms.MaskedTextBox();
            this.c_radius = new System.Windows.Forms.Label();
            this.c_y1 = new System.Windows.Forms.Label();
            this.c_x1 = new System.Windows.Forms.Label();
            this.Circle = new System.Windows.Forms.Label();
            this.clear = new System.Windows.Forms.Button();
            this.zoomminus = new System.Windows.Forms.Button();
            this.zoomplus = new System.Windows.Forms.Button();
            this.penColor = new System.Windows.Forms.Panel();
            this.penWidth = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.fileNameStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Line_Pannel.SuspendLayout();
            this.Rect_Pannel.SuspendLayout();
            this.Circle_Pannel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "Line",
            "Rectangle",
            "Circle"});
            this.listBox1.Location = new System.Drawing.Point(13, 27);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(127, 43);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // Line_Pannel
            // 
            this.Line_Pannel.Controls.Add(this.lt_y2);
            this.Line_Pannel.Controls.Add(this.lt_y1);
            this.Line_Pannel.Controls.Add(this.lt_x2);
            this.Line_Pannel.Controls.Add(this.lt_x1);
            this.Line_Pannel.Controls.Add(this.l_y2);
            this.Line_Pannel.Controls.Add(this.l_x2);
            this.Line_Pannel.Controls.Add(this.l_y1);
            this.Line_Pannel.Controls.Add(this.l_x1);
            this.Line_Pannel.Controls.Add(this.Linie);
            this.Line_Pannel.Enabled = false;
            this.Line_Pannel.Location = new System.Drawing.Point(13, 88);
            this.Line_Pannel.Name = "Line_Pannel";
            this.Line_Pannel.Size = new System.Drawing.Size(232, 113);
            this.Line_Pannel.TabIndex = 1;
            // 
            // lt_y2
            // 
            this.lt_y2.Location = new System.Drawing.Point(133, 70);
            this.lt_y2.Mask = "9999";
            this.lt_y2.Name = "lt_y2";
            this.lt_y2.Size = new System.Drawing.Size(37, 20);
            this.lt_y2.TabIndex = 12;
            // 
            // lt_y1
            // 
            this.lt_y1.Location = new System.Drawing.Point(133, 38);
            this.lt_y1.Mask = "9999";
            this.lt_y1.Name = "lt_y1";
            this.lt_y1.Size = new System.Drawing.Size(34, 20);
            this.lt_y1.TabIndex = 11;
            // 
            // lt_x2
            // 
            this.lt_x2.Location = new System.Drawing.Point(42, 70);
            this.lt_x2.Mask = "9999";
            this.lt_x2.Name = "lt_x2";
            this.lt_x2.Size = new System.Drawing.Size(35, 20);
            this.lt_x2.TabIndex = 10;
            // 
            // lt_x1
            // 
            this.lt_x1.Location = new System.Drawing.Point(42, 38);
            this.lt_x1.Mask = "9999";
            this.lt_x1.Name = "lt_x1";
            this.lt_x1.Size = new System.Drawing.Size(35, 20);
            this.lt_x1.TabIndex = 9;
            // 
            // l_y2
            // 
            this.l_y2.AutoSize = true;
            this.l_y2.Location = new System.Drawing.Point(106, 73);
            this.l_y2.Name = "l_y2";
            this.l_y2.Size = new System.Drawing.Size(21, 13);
            this.l_y2.TabIndex = 4;
            this.l_y2.Text = "y2:";
            // 
            // l_x2
            // 
            this.l_x2.AutoSize = true;
            this.l_x2.Location = new System.Drawing.Point(16, 73);
            this.l_x2.Name = "l_x2";
            this.l_x2.Size = new System.Drawing.Size(21, 13);
            this.l_x2.TabIndex = 3;
            this.l_x2.Text = "x2:";
            // 
            // l_y1
            // 
            this.l_y1.AutoSize = true;
            this.l_y1.Location = new System.Drawing.Point(106, 41);
            this.l_y1.Name = "l_y1";
            this.l_y1.Size = new System.Drawing.Size(21, 13);
            this.l_y1.TabIndex = 2;
            this.l_y1.Text = "y1:";
            // 
            // l_x1
            // 
            this.l_x1.AutoSize = true;
            this.l_x1.Location = new System.Drawing.Point(16, 41);
            this.l_x1.Name = "l_x1";
            this.l_x1.Size = new System.Drawing.Size(21, 13);
            this.l_x1.TabIndex = 1;
            this.l_x1.Text = "x1:";
            // 
            // Linie
            // 
            this.Linie.AutoSize = true;
            this.Linie.Location = new System.Drawing.Point(13, 14);
            this.Linie.Name = "Linie";
            this.Linie.Size = new System.Drawing.Size(29, 13);
            this.Linie.TabIndex = 0;
            this.Linie.Text = "Linie";
            // 
            // draw_Pannel
            // 
            this.draw_Pannel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.draw_Pannel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.draw_Pannel.Location = new System.Drawing.Point(271, 27);
            this.draw_Pannel.Name = "draw_Pannel";
            this.draw_Pannel.Size = new System.Drawing.Size(619, 547);
            this.draw_Pannel.TabIndex = 2;
            this.draw_Pannel.Paint += new System.Windows.Forms.PaintEventHandler(this.draw_Pannel_Paint);
            this.draw_Pannel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.draw_Pannel_MouseClick);
            // 
            // draw
            // 
            this.draw.Location = new System.Drawing.Point(32, 489);
            this.draw.Name = "draw";
            this.draw.Size = new System.Drawing.Size(189, 30);
            this.draw.TabIndex = 3;
            this.draw.Text = "Draw";
            this.draw.UseVisualStyleBackColor = true;
            this.draw.Click += new System.EventHandler(this.draw_Click);
            // 
            // Rect_Pannel
            // 
            this.Rect_Pannel.Controls.Add(this.rt_height);
            this.Rect_Pannel.Controls.Add(this.rt_width);
            this.Rect_Pannel.Controls.Add(this.rt_y2);
            this.Rect_Pannel.Controls.Add(this.rt_y1);
            this.Rect_Pannel.Controls.Add(this.rt_x2);
            this.Rect_Pannel.Controls.Add(this.rt_x1);
            this.Rect_Pannel.Controls.Add(this.r_height);
            this.Rect_Pannel.Controls.Add(this.r_width);
            this.Rect_Pannel.Controls.Add(this.r_y2);
            this.Rect_Pannel.Controls.Add(this.r_y1);
            this.Rect_Pannel.Controls.Add(this.r_x2);
            this.Rect_Pannel.Controls.Add(this.r_x1);
            this.Rect_Pannel.Controls.Add(this.Rectangle);
            this.Rect_Pannel.Enabled = false;
            this.Rect_Pannel.Location = new System.Drawing.Point(13, 220);
            this.Rect_Pannel.Name = "Rect_Pannel";
            this.Rect_Pannel.Size = new System.Drawing.Size(232, 128);
            this.Rect_Pannel.TabIndex = 4;
            // 
            // rt_height
            // 
            this.rt_height.Location = new System.Drawing.Point(150, 97);
            this.rt_height.Mask = "9999";
            this.rt_height.Name = "rt_height";
            this.rt_height.Size = new System.Drawing.Size(37, 20);
            this.rt_height.TabIndex = 18;
            this.rt_height.TextChanged += new System.EventHandler(this.rt_height_TextChanged);
            // 
            // rt_width
            // 
            this.rt_width.Location = new System.Drawing.Point(54, 97);
            this.rt_width.Mask = "9999";
            this.rt_width.Name = "rt_width";
            this.rt_width.Size = new System.Drawing.Size(38, 20);
            this.rt_width.TabIndex = 17;
            this.rt_width.TextChanged += new System.EventHandler(this.rt_width_TextChanged);
            // 
            // rt_y2
            // 
            this.rt_y2.Location = new System.Drawing.Point(133, 66);
            this.rt_y2.Mask = "9999";
            this.rt_y2.Name = "rt_y2";
            this.rt_y2.Size = new System.Drawing.Size(37, 20);
            this.rt_y2.TabIndex = 16;
            this.rt_y2.TextChanged += new System.EventHandler(this.rt_y2_TextChanged);
            // 
            // rt_y1
            // 
            this.rt_y1.Location = new System.Drawing.Point(133, 37);
            this.rt_y1.Mask = "9999";
            this.rt_y1.Name = "rt_y1";
            this.rt_y1.Size = new System.Drawing.Size(35, 20);
            this.rt_y1.TabIndex = 15;
            // 
            // rt_x2
            // 
            this.rt_x2.Location = new System.Drawing.Point(42, 66);
            this.rt_x2.Mask = "9999";
            this.rt_x2.Name = "rt_x2";
            this.rt_x2.Size = new System.Drawing.Size(36, 20);
            this.rt_x2.TabIndex = 14;
            this.rt_x2.TextChanged += new System.EventHandler(this.rt_x2_TextChanged);
            // 
            // rt_x1
            // 
            this.rt_x1.Location = new System.Drawing.Point(42, 37);
            this.rt_x1.Mask = "9999";
            this.rt_x1.Name = "rt_x1";
            this.rt_x1.Size = new System.Drawing.Size(36, 20);
            this.rt_x1.TabIndex = 13;
            // 
            // r_height
            // 
            this.r_height.AutoSize = true;
            this.r_height.Location = new System.Drawing.Point(105, 100);
            this.r_height.Name = "r_height";
            this.r_height.Size = new System.Drawing.Size(39, 13);
            this.r_height.TabIndex = 10;
            this.r_height.Text = "height:";
            // 
            // r_width
            // 
            this.r_width.AutoSize = true;
            this.r_width.Location = new System.Drawing.Point(16, 100);
            this.r_width.Name = "r_width";
            this.r_width.Size = new System.Drawing.Size(35, 13);
            this.r_width.TabIndex = 9;
            this.r_width.Text = "width:";
            // 
            // r_y2
            // 
            this.r_y2.AutoSize = true;
            this.r_y2.Location = new System.Drawing.Point(106, 69);
            this.r_y2.Name = "r_y2";
            this.r_y2.Size = new System.Drawing.Size(21, 13);
            this.r_y2.TabIndex = 4;
            this.r_y2.Text = "y2:";
            // 
            // r_y1
            // 
            this.r_y1.AutoSize = true;
            this.r_y1.Location = new System.Drawing.Point(106, 40);
            this.r_y1.Name = "r_y1";
            this.r_y1.Size = new System.Drawing.Size(21, 13);
            this.r_y1.TabIndex = 3;
            this.r_y1.Text = "y1:";
            // 
            // r_x2
            // 
            this.r_x2.AutoSize = true;
            this.r_x2.Location = new System.Drawing.Point(16, 69);
            this.r_x2.Name = "r_x2";
            this.r_x2.Size = new System.Drawing.Size(21, 13);
            this.r_x2.TabIndex = 2;
            this.r_x2.Text = "x2:";
            // 
            // r_x1
            // 
            this.r_x1.AutoSize = true;
            this.r_x1.Location = new System.Drawing.Point(16, 40);
            this.r_x1.Name = "r_x1";
            this.r_x1.Size = new System.Drawing.Size(21, 13);
            this.r_x1.TabIndex = 1;
            this.r_x1.Text = "x1:";
            // 
            // Rectangle
            // 
            this.Rectangle.AutoSize = true;
            this.Rectangle.Location = new System.Drawing.Point(16, 16);
            this.Rectangle.Name = "Rectangle";
            this.Rectangle.Size = new System.Drawing.Size(56, 13);
            this.Rectangle.TabIndex = 0;
            this.Rectangle.Text = "Rectangle";
            // 
            // Circle_Pannel
            // 
            this.Circle_Pannel.Controls.Add(this.ct_radius);
            this.Circle_Pannel.Controls.Add(this.ct_y1);
            this.Circle_Pannel.Controls.Add(this.ct_x1);
            this.Circle_Pannel.Controls.Add(this.c_radius);
            this.Circle_Pannel.Controls.Add(this.c_y1);
            this.Circle_Pannel.Controls.Add(this.c_x1);
            this.Circle_Pannel.Controls.Add(this.Circle);
            this.Circle_Pannel.Enabled = false;
            this.Circle_Pannel.Location = new System.Drawing.Point(13, 370);
            this.Circle_Pannel.Name = "Circle_Pannel";
            this.Circle_Pannel.Size = new System.Drawing.Size(232, 113);
            this.Circle_Pannel.TabIndex = 5;
            // 
            // ct_radius
            // 
            this.ct_radius.Location = new System.Drawing.Point(88, 72);
            this.ct_radius.Mask = "9999";
            this.ct_radius.Name = "ct_radius";
            this.ct_radius.Size = new System.Drawing.Size(39, 20);
            this.ct_radius.TabIndex = 14;
            // 
            // ct_y1
            // 
            this.ct_y1.Location = new System.Drawing.Point(132, 42);
            this.ct_y1.Mask = "9999";
            this.ct_y1.Name = "ct_y1";
            this.ct_y1.Size = new System.Drawing.Size(38, 20);
            this.ct_y1.TabIndex = 13;
            // 
            // ct_x1
            // 
            this.ct_x1.Location = new System.Drawing.Point(43, 42);
            this.ct_x1.Mask = "9999";
            this.ct_x1.Name = "ct_x1";
            this.ct_x1.Size = new System.Drawing.Size(39, 20);
            this.ct_x1.TabIndex = 12;
            // 
            // c_radius
            // 
            this.c_radius.AutoSize = true;
            this.c_radius.Location = new System.Drawing.Point(39, 75);
            this.c_radius.Name = "c_radius";
            this.c_radius.Size = new System.Drawing.Size(43, 13);
            this.c_radius.TabIndex = 11;
            this.c_radius.Text = "Radius:";
            // 
            // c_y1
            // 
            this.c_y1.AutoSize = true;
            this.c_y1.Location = new System.Drawing.Point(105, 45);
            this.c_y1.Name = "c_y1";
            this.c_y1.Size = new System.Drawing.Size(21, 13);
            this.c_y1.TabIndex = 10;
            this.c_y1.Text = "y1:";
            // 
            // c_x1
            // 
            this.c_x1.AutoSize = true;
            this.c_x1.Location = new System.Drawing.Point(16, 45);
            this.c_x1.Name = "c_x1";
            this.c_x1.Size = new System.Drawing.Size(21, 13);
            this.c_x1.TabIndex = 9;
            this.c_x1.Text = "x1:";
            // 
            // Circle
            // 
            this.Circle.AutoSize = true;
            this.Circle.Location = new System.Drawing.Point(16, 17);
            this.Circle.Name = "Circle";
            this.Circle.Size = new System.Drawing.Size(33, 13);
            this.Circle.TabIndex = 9;
            this.Circle.Text = "Circle";
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(32, 525);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(189, 35);
            this.clear.TabIndex = 6;
            this.clear.Text = "Clear";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // zoomminus
            // 
            this.zoomminus.Location = new System.Drawing.Point(251, 495);
            this.zoomminus.Name = "zoomminus";
            this.zoomminus.Size = new System.Drawing.Size(14, 19);
            this.zoomminus.TabIndex = 7;
            this.zoomminus.Text = "-";
            this.zoomminus.UseVisualStyleBackColor = true;
            this.zoomminus.Click += new System.EventHandler(this.zoomminus_Click);
            // 
            // zoomplus
            // 
            this.zoomplus.Location = new System.Drawing.Point(231, 495);
            this.zoomplus.Name = "zoomplus";
            this.zoomplus.Size = new System.Drawing.Size(14, 19);
            this.zoomplus.TabIndex = 8;
            this.zoomplus.Text = "+";
            this.zoomplus.UseVisualStyleBackColor = true;
            this.zoomplus.Click += new System.EventHandler(this.zoomplus_Click);
            // 
            // penColor
            // 
            this.penColor.BackColor = System.Drawing.Color.Black;
            this.penColor.Location = new System.Drawing.Point(222, 54);
            this.penColor.Name = "penColor";
            this.penColor.Size = new System.Drawing.Size(43, 20);
            this.penColor.TabIndex = 110;
            this.penColor.Click += new System.EventHandler(this.penColor_Click);
            // 
            // penWidth
            // 
            this.penWidth.Location = new System.Drawing.Point(221, 28);
            this.penWidth.Mask = "9999px";
            this.penWidth.Name = "penWidth";
            this.penWidth.Size = new System.Drawing.Size(43, 20);
            this.penWidth.TabIndex = 109;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(146, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 108;
            this.label15.Text = "Pen Color: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(146, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 107;
            this.label11.Text = "Pen Width:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileNameStatusLabel,
            this.statusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 573);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(902, 22);
            this.statusStrip1.TabIndex = 111;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // fileNameStatusLabel
            // 
            this.fileNameStatusLabel.Name = "fileNameStatusLabel";
            this.fileNameStatusLabel.Size = new System.Drawing.Size(116, 17);
            this.fileNameStatusLabel.Text = "Unbekanntes Projekt";
            // 
            // statusLabel1
            // 
            this.statusLabel1.Name = "statusLabel1";
            this.statusLabel1.Size = new System.Drawing.Size(114, 17);
            this.statusLabel1.Text = "2014 Hauer Dominik";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(902, 24);
            this.menuStrip1.TabIndex = 112;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click_1);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click_1);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click_1);
            // 
            // Mini_CAD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 595);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.penColor);
            this.Controls.Add(this.penWidth);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.zoomplus);
            this.Controls.Add(this.zoomminus);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.Circle_Pannel);
            this.Controls.Add(this.Rect_Pannel);
            this.Controls.Add(this.draw);
            this.Controls.Add(this.draw_Pannel);
            this.Controls.Add(this.Line_Pannel);
            this.Controls.Add(this.listBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Mini_CAD";
            this.Text = "Mini_CAD";
            this.Line_Pannel.ResumeLayout(false);
            this.Line_Pannel.PerformLayout();
            this.Rect_Pannel.ResumeLayout(false);
            this.Rect_Pannel.PerformLayout();
            this.Circle_Pannel.ResumeLayout(false);
            this.Circle_Pannel.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel Line_Pannel;
        private System.Windows.Forms.Label Linie;
        private System.Windows.Forms.Label l_y2;
        private System.Windows.Forms.Label l_x2;
        private System.Windows.Forms.Label l_y1;
        private System.Windows.Forms.Label l_x1;
        private System.Windows.Forms.Panel draw_Pannel;
        private System.Windows.Forms.Button draw;
        private System.Windows.Forms.Panel Rect_Pannel;
        private System.Windows.Forms.Label Rectangle;
        private System.Windows.Forms.Label r_height;
        private System.Windows.Forms.Label r_width;
        private System.Windows.Forms.Label r_y2;
        private System.Windows.Forms.Label r_y1;
        private System.Windows.Forms.Label r_x2;
        private System.Windows.Forms.Label r_x1;
        private System.Windows.Forms.Panel Circle_Pannel;
        private System.Windows.Forms.Label c_radius;
        private System.Windows.Forms.Label c_y1;
        private System.Windows.Forms.Label c_x1;
        private System.Windows.Forms.Label Circle;
        private System.Windows.Forms.MaskedTextBox lt_y2;
        private System.Windows.Forms.MaskedTextBox lt_y1;
        private System.Windows.Forms.MaskedTextBox lt_x2;
        private System.Windows.Forms.MaskedTextBox lt_x1;
        private System.Windows.Forms.MaskedTextBox rt_height;
        private System.Windows.Forms.MaskedTextBox rt_width;
        private System.Windows.Forms.MaskedTextBox rt_y2;
        private System.Windows.Forms.MaskedTextBox rt_y1;
        private System.Windows.Forms.MaskedTextBox rt_x2;
        private System.Windows.Forms.MaskedTextBox rt_x1;
        private System.Windows.Forms.MaskedTextBox ct_radius;
        private System.Windows.Forms.MaskedTextBox ct_y1;
        private System.Windows.Forms.MaskedTextBox ct_x1;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button zoomminus;
        private System.Windows.Forms.Button zoomplus;
        private System.Windows.Forms.Panel penColor;
        private System.Windows.Forms.MaskedTextBox penWidth;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel fileNameStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
    }
}

