﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace Mini_CAD
{
    class Circle:Shapes
    {
        int radius;

        public Circle(int x1, int y1, int radius,Color color, int lineWidth):base(x1, y1,color,lineWidth)
        {
            this.radius = radius;
        }

        public override void Draw(Graphics g)
        {
            g.DrawEllipse(new Pen(color,lineWidth), x1-radius, y1-radius, radius*2, radius*2);
            
            if (selected)
            {
                g.DrawRectangle(Pens.Red, x1 - 5 - radius, y1 - 5, 10, 10);
                g.DrawRectangle(Pens.Red, x1 - 5, y1 -5  - radius,10,10);
                g.DrawRectangle(Pens.Red, x1 - 5 +radius, y1 - 5, 10, 10);
                g.DrawRectangle(Pens.Red, x1 - 5, y1 - 5 + radius, 10, 10);
            }
        }

        public override bool  AmISelected(int x, int y)
        {
            if(x1+radius >= x && y1+radius >= y && x1+radius*2 >= x && y1+radius*2 >=y)
            
            {
                selected = true;
                return selected;
            }
            else
            {
                selected = false;
                return selected;
            }
        }

        public Point GetMiddlePoint()
        {
            return new Point(x1, y1); ;
        }

        public float GetRadius()
        {
            return radius;
        }

        public override string GetSaveString()
        {
            return "Circle;" + "x1=" + x1 + ";y1=" + y1 + ";radius=" + radius + ";colorR=" + color.R + ";colorG=" + color.G + ";colorB=" + color.B + ";penWidth=" + lineWidth;
        }
    }
}
