﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mini_CAD
{
    abstract class Shapes:IDraw
    {
        protected int x1;
        protected int y1;
        protected Color color;
        protected int lineWidth;
        public bool selected = false;


        public Shapes(int x1, int y1, Color color, int lineWidth)
        {
            this.x1 = x1;
            this.y1 = y1;
            this.color = color;
            this.lineWidth = lineWidth;
        }

        public static Shapes LoadFromString(string s)
        {
            List<string> props = s.Split(';').ToList();
            Shapes shape = null;

            if (props[0] == "Line")
            {
                props.RemoveAt(0);
                int x1 = 0;
                int x2 = 0;
                int y1 = 0;
                int y2 = 0;
                byte r = 0, g = 0, b = 0;
                float lineWidth = 0;

                #region LineSwitch
                foreach (string st in props)
                {
                    string buff = st.Split('=')[1];
                    switch (st.Split('=')[0])
                    {
                        case "x1":
                            int xs;
                            if (!int.TryParse(buff, out xs))
                                MessageBox.Show("Invalid Argument: " + st);
                            x1=xs;
                            break;
                        case "y1":
                            int ys;
                            if (!int.TryParse(buff, out ys))
                                MessageBox.Show("Invalid Argument: " + st);
                            y1 = ys;
                            break;
                        case "x2":
                            int xe;
                            if (!int.TryParse(buff, out xe))
                                MessageBox.Show("Invalid Argument: " + st);
                            x2 = xe;
                            break;
                        case "y2":
                            int ye;
                            if (!int.TryParse(buff, out ye))
                                MessageBox.Show("Invalid Argument: " + st);
                            y2 = ye;
                            break;
                        case "colorR":
                            if (!byte.TryParse(buff, out r))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorG":
                            if (!byte.TryParse(buff, out g))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorB":
                            if (!byte.TryParse(buff, out b))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "penWidth":
                            if (!float.TryParse(buff, out lineWidth))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;

                        default:
                            MessageBox.Show("Invalid Argument: " + st);
                            break;
                    }
                }
                #endregion

                shape = new Line(x1,x2,y1,y2,Color.FromArgb(r, g, b), (int)lineWidth); 

            }
            else if (props[0] == "Rectangle")
            {
                props.RemoveAt(0);
                int x1 = 0;
                int y1 = 0;
                int width = 0;
                int heigth = 0;
                byte r = 0, g = 0, b = 0;
                float lineWidth = 0;

                #region RectSwitch
                foreach (string st in props)
                {
                    string buff = st.Split('=')[1];
                    switch (st.Split('=')[0])
                    {
                        case "x1":
                            int xs;
                            if (!int.TryParse(buff, out xs))
                                MessageBox.Show("Invalid Argument: " + st);
                            x1 = xs;
                            break;
                        case "y1":
                            int ys;
                            if (!int.TryParse(buff, out ys))
                                MessageBox.Show("Invalid Argument: " + st);
                            y1 = ys;
                            break;
                        case "heigth":
                            int xe;
                            if (!int.TryParse(buff, out xe))
                                MessageBox.Show("Invalid Argument: " + st);
                            heigth = xe;
                            break;
                        case "width":
                            int ye;
                            if (!int.TryParse(buff, out ye))
                                MessageBox.Show("Invalid Argument: " + st);
                            width = ye;
                            break;
                        case "colorR":
                            if (!byte.TryParse(buff, out r))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorG":
                            if (!byte.TryParse(buff, out g))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorB":
                            if (!byte.TryParse(buff, out b))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "penWidth":
                            if (!float.TryParse(buff, out lineWidth))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;

                        default:
                            MessageBox.Show("Invalid Argument: " + st);
                            break;
                    }
                }
                #endregion

                shape = new Rectangle( x1,y1,width,heigth,Color.FromArgb(r, g, b), (int)lineWidth);
            }
            else if (props[0] == "Circle")
            {
                props.RemoveAt(0);
                int x1 = 0;
                int y1 = 0; 
                byte r = 0, g = 0, b = 0;
                float radius = 0;

                float lineWidth = 0;

                #region CircleSwitch
                foreach (string st in props)
                {
                    string buff = st.Split('=')[1];
                    switch (st.Split('=')[0])
                    {
                        case "x1":
                            int x;
                            if (!int.TryParse(buff, out x))
                                MessageBox.Show("Invalid Argument: " + st);
                            x1 = x;
                            break;
                        case "y2":
                            int y;
                            if (!int.TryParse(buff, out y))
                                MessageBox.Show("Invalid Argument: " + st);
                            y1 = y;
                            break;
                        case "radius":
                            if (!float.TryParse(buff, out radius))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorR":
                            if (!byte.TryParse(buff, out r))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorG":
                            if (!byte.TryParse(buff, out g))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorB":
                            if (!byte.TryParse(buff, out b))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "penWidth":
                            if (!float.TryParse(buff, out lineWidth))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;

                        default:
                            MessageBox.Show("Invalid Argument: " + st);
                            break;
                    }
                }
                #endregion

                shape = new Circle(x1,y1,(int)radius,Color.FromArgb(r, g, b), (int)lineWidth);
            }

            return shape;
        }

        public abstract void Draw(Graphics g);

        public abstract bool AmISelected(int x, int y);

        public abstract string GetSaveString();


        public Color GetColor()
        {
            return color;
        }

        public int GetLineWidth()
        {
            return lineWidth;
        }
   }
}
