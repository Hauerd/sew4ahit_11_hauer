﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace Mini_CAD
{
    interface IDraw
    {
        void Draw(Graphics g);

        bool AmISelected(int x, int y);
    }
}
