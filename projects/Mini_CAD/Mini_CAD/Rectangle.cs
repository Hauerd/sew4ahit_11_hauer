﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Mini_CAD
{
    class Rectangle:Shapes
    {
        int width;
        int heigth;

        public Rectangle(int x1, int y1, int width, int heigth,Color color, int lineWidth):base(x1, y1,color,lineWidth)
        {
            this.width = width;
            this.heigth = heigth;
        }

        public override void Draw(Graphics g)
        {
            g.DrawRectangle(new Pen(color,lineWidth), x1, y1, width, heigth);

            if (selected)
            {
                g.DrawRectangle(Pens.Red, x1 - 5, y1 - 5, 10, 10);
                g.DrawRectangle(Pens.Red, x1 - 5, y1 + heigth - 5, 10, 10);
                g.DrawRectangle(Pens.Red, x1 + width - 5, y1 - 5, 10, 10);
                g.DrawRectangle(Pens.Red, x1 + width - 5, y1 + heigth - 5, 10, 10);

                selected = false;
            }
        }

        public override bool AmISelected(int x, int y)
        {

            if (x >= x1 && x <= x1 + width && y >= y1 && y <= y1 + heigth)
            {
                selected = true;
                return selected;
            }
            else
            {
                selected = false;
                return selected;
            }
            
        }

        public Point GetTopLeftPoint()
        {
            return new Point(x1,y1);
        }

        public Point GetBottomRightPoint()
        {
            return new Point(x1+width,y1+heigth);
        }

        public override string GetSaveString()
        {
            return "Rectangle;" + "x1=" + x1 + ";y1=" + y1 + ";heigth=" + heigth + ";width=" + width + ";colorR=" + color.R + ";colorG=" + color.G + ";colorB=" + color.B + ";penWidth=" + lineWidth;
        }
    }
}
