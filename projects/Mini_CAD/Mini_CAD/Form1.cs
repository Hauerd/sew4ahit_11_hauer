﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;

namespace Mini_CAD
{
    public partial class Mini_CAD : Form
    {
        List<Shapes> shapes;
        double zoomFactor;


        public Mini_CAD()
        {
            InitializeComponent();
            clear.Enabled = false;

            this.shapes = new List<Shapes>();
            this.draw_Pannel.Paint += new PaintEventHandler(draw_Pannel_Paint);

            this.zoomFactor = 1.0;
            this.penWidth.Text = "1";
            listBox1.SelectedIndex = 0;
        }

        void showProperties(Shapes s)
        {
            Type t = s.GetType();

            if (t == typeof(Line))
            {
                Line l = (Line)s;
                listBox1.SelectedIndex = 0;
                lt_x1.Text = l.GetStartPoint().X.ToString();
                lt_y1.Text = l.GetStartPoint().Y.ToString();
                lt_x2.Text = l.GetEndPoint().X.ToString();
                lt_y2.Text = l.GetEndPoint().Y.ToString();
            }
            else if (t == typeof(Circle))
            {
                Circle c = (Circle)s;
                listBox1.SelectedIndex = 2;
                ct_x1.Text = c.GetMiddlePoint().X.ToString();
                ct_y1.Text = c.GetMiddlePoint().Y.ToString();
                ct_radius.Text = c.GetRadius().ToString();
            }
            else if (t == typeof(Rectangle))
            {
                Rectangle r = (Rectangle)s;
                listBox1.SelectedIndex = 1;
                rt_x1.Text = r.GetTopLeftPoint().X.ToString();
                rt_y1.Text = r.GetTopLeftPoint().Y.ToString();
                rt_x2.Text = r.GetBottomRightPoint().X.ToString();
                rt_y2.Text = r.GetBottomRightPoint().Y.ToString();
            }
            penWidth.Text = s.GetLineWidth().ToString();
            penColor.BackColor = s.GetColor();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            if (listBox1.SelectedIndex == 0)
            {
                Line_Pannel.Enabled = true;
                Rect_Pannel.Enabled = false;
                Circle_Pannel.Enabled = false;
            }
            if (listBox1.SelectedIndex == 1)
            {
                Rect_Pannel.Enabled = true;
                Circle_Pannel.Enabled = false;
                Line_Pannel.Enabled = false;
            }
            if (listBox1.SelectedIndex == 2)
            {
                Circle_Pannel.Enabled = true;
                Rect_Pannel.Enabled = false;
                Line_Pannel.Enabled = false;
            }
        }


        private void draw_Pannel_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Transform = new Matrix((float)zoomFactor, 0, 0, (float)zoomFactor, 0, 0); 
            foreach (Shapes s in this.shapes)
                s.Draw(e.Graphics);
            GetIntersection(e.Graphics);
        }

        private void draw_Click(object sender, EventArgs e)
        {
            #region Line
            if (listBox1.SelectedIndex == 0)
            {
                if (this.lt_x1.Text != "" && this.lt_y1.Text != "" && this.lt_x2.Text != "" && this.lt_y2.Text != "")
                {
                    int lx1 = int.Parse(this.lt_x1.Text);
                    int lx2 = int.Parse(this.lt_x2.Text);
                    int ly1 = int.Parse(this.lt_y1.Text);
                    int ly2 = int.Parse(this.lt_y2.Text);

                    if(lx1 <= lx2 && ly1 <= ly2 && lx1 >= 0 && ly1 >= 0)                  
                    {
                        this.shapes.Add(new Line(lx1, lx2, ly1, ly2, penColor.BackColor, int.Parse(penWidth.Text.Replace("px", ""))));
                    }

                    else{
                    MessageBox.Show("Falsche Zahlenwerte eingegeben!");
                    }
                }

                else{
                    MessageBox.Show("Falsche Eingabe");
                }
            }
            #endregion

            #region Rectangle
            if (listBox1.SelectedIndex == 1)
            {
                if (this.rt_x1.Text != "" && this.rt_y1.Text != "")
                {
                    int rx1 = int.Parse(this.rt_x1.Text);
                    int ry1 = int.Parse(this.rt_y1.Text);
                    int rx2 = 0;
                    int ry2 = 0;
                    int heigth = 0;
                    int width = 0;


                    if (this.rt_x2.Text == "" || this.rt_y2.Text == "")
                    {
                        if (this.rt_height.Text != "" && this.rt_width.Text != "")
                        {
                            heigth = int.Parse(this.rt_height.Text);
                            width = int.Parse(this.rt_width.Text);
                        }
                    }

                    else if (this.rt_height.Text == "" || this.rt_width.Text == "")
                    {
                        if (this.rt_x2.Text != "" || this.rt_y2.Text != "")
                        {
                            rx2 = int.Parse(this.rt_x2.Text);
                            ry2 = int.Parse(this.rt_y2.Text);
                            heigth = Math.Abs(int.Parse(rt_y2.Text) - int.Parse(rt_y1.Text));
                            width = Math.Abs(int.Parse(rt_x2.Text) - int.Parse(rt_x1.Text));
                        }
                    }
                    if (heigth > 0 && width >= 0)
                    {

                        this.shapes.Add(new Rectangle(rx1, ry1, width, heigth, penColor.BackColor, int.Parse(penWidth.Text.Replace("px", ""))));
                    }
                    else
                    {
                        MessageBox.Show("Minus Werte eingegeben!");
                    }
                }
                else
                {
                    MessageBox.Show("Falsche Eingabe");
                }
            }
            #endregion

            #region Circle
            if (listBox1.SelectedIndex == 2)
            {
                if (this.ct_x1.Text != "" && this.ct_y1.Text != "" && this.ct_radius.Text != "")
                {
                    int cx1 = int.Parse(this.ct_x1.Text);
                    int cy1 = int.Parse(this.ct_y1.Text);
                    int radius = int.Parse(this.ct_radius.Text);
                    this.shapes.Add(new Circle(cx1, cy1, radius, penColor.BackColor, int.Parse(penWidth.Text.Replace("px", ""))));
                }
                else
                {
                    MessageBox.Show("Falsche Eingabe");
                }
            }
            #endregion

            this.draw_Pannel.Invalidate();
        }

        private void rt_width_TextChanged(object sender, EventArgs e)
        {
            if (rt_width.Text == "")
            {
                rt_x2.Enabled = true;
                rt_y2.Enabled = true;
            }
            else
            {
                rt_x2.Enabled = false;
                rt_y2.Enabled = false;

            }
        }

        private void rt_height_TextChanged(object sender, EventArgs e)
        {
            if (rt_height.Text == "")
            {
                rt_x2.Enabled = true;
                rt_y2.Enabled = true;
            }
            else
            {
                rt_x2.Enabled = false;
                rt_y2.Enabled = false;
            }
        }

        private void rt_x2_TextChanged(object sender, EventArgs e)
        {
            if (rt_x2.Text == "")
            {
                rt_height.Enabled = true;
                rt_width.Enabled = true;
            }
            else
            {
                rt_height.Enabled = false;
                rt_width.Enabled = false;
            }
        }

        private void rt_y2_TextChanged(object sender, EventArgs e)
        {
            if (rt_y2.Text == "")
            {
                rt_height.Enabled = true;
                rt_width.Enabled = true;
            }
            else
            {
                rt_height.Enabled = false;
                rt_width.Enabled = false;
            }
        }

        private void draw_Pannel_MouseClick(object sender, MouseEventArgs e)
        {

            foreach (Shapes s in this.shapes)
            {
                s.selected = false;
                clear.Enabled = false;
            }

            int selectx = e.X;
            int selecty = e.Y;

            foreach (Shapes s in this.shapes)
            {
                if (s.AmISelected(selectx, selecty))
                {
                    s.selected = true;
                    clear.Enabled = true;
                    showProperties(s);
                    break;
                }
            }
            this.draw_Pannel.Invalidate();
        }

        private void clear_Click(object sender, EventArgs e)
        {
            Shapes s = shapes.Find(sa => sa.selected);
            shapes.Remove(s);
            draw_Pannel.Invalidate();
        }

        private void zoomplus_Click(object sender, EventArgs e)
        {
            zoomFactor += 0.5;
            if (zoomFactor > 5)
                zoomFactor = 5;

            this.draw_Pannel.Invalidate();
            
        }

        private void zoomminus_Click(object sender, EventArgs e)
        {
            zoomFactor -= 0.5;

            if (zoomFactor < 1)
                zoomFactor = 1;

            this.draw_Pannel.Invalidate();
        }

        private void penColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                penColor.BackColor = colorDialog1.Color;
        }


        private void SaveProject(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
            StreamWriter sw = new StreamWriter(fileName, false);
            string buffer = "";
            foreach (Shapes s in shapes)
                buffer += (s.GetSaveString() + "\r\n");
           
            sw.WriteLine(buffer);
            sw.Close();

            fileNameStatusLabel.Text = Path.GetFileName(fileName);
        }

        private void LoadProject(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            shapes = new List<Shapes>();

            string buffer;
            while ((buffer = sr.ReadLine()) != null)
                shapes.Add(Shapes.LoadFromString(buffer));
            
            shapes.RemoveAll((c) => c == null);
            sr.Close();

            draw_Pannel.Invalidate();
            fileNameStatusLabel.Text = Path.GetFileName(fileName);
            fileNameStatusLabel.Tag = fileName;
        }

        private void saveToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sd = new SaveFileDialog();

            sd.Filter = "CAD-Datei|*.cad";

            if (fileNameStatusLabel.Text != "Unbekanntes Projekt")
                SaveProject(fileNameStatusLabel.Tag.ToString());
            else if (sd.ShowDialog() == DialogResult.OK)
                SaveProject(sd.FileName);
        }

        private void loadToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();

            od.Filter = "CAD-Datei|*.cad";

            if (od.ShowDialog() == DialogResult.OK)
                LoadProject(od.FileName);
        }

        private void newToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            shapes.Clear();
            fileNameStatusLabel.Text = "Unbekanntes Projekt";
            draw_Pannel.Invalidate();
        }

        private void GetIntersection(Graphics g)
        {
            List<Line> ll = new List<Line>();
            int icount = 0;
            for (int i = 0; i < shapes.Count(); i++)
            {
                if (shapes[i].GetType() == typeof(Line))
                {
                    icount++;
                    ll.Add((Line)shapes[i]);
                }
            }
            Pen pnow = new Pen(Color.Coral, 5F);
            Font f1 = new Font("Verdana", 10F);
            if (ll.Count >= 2)
                for (int i = 0; i < ll.Count(); i++)
                {
                    for (int j = i + 1; j < ll.Count(); j++)
                    {
                        Line l1 = ll[i];
                        Line l2 = ll[j];

                        double dk1 = 0;
                        double dk2 = 0;

                        Point end1 = l1.GetEndPoint();
                        Point start1 = l1.GetStartPoint();

                        Point end2 = l2.GetEndPoint();
                        Point start2 = l2.GetStartPoint();

                        if (end1.X == start1.X)
                        {

                            dk1 = 9999999;
                        }
                        else
                            dk1 = (end1.Y - start1.Y) / (end1.X - start1.X);

                        if (end2.X == start2.X)
                        {
                            dk2 = 9999999;
                        }
                        else
                            dk2 = (end2.Y - start2.Y) / (end2.X - start2.X);

                        double dd1 = start1.Y - (dk1 * start1.X);
                        double dd2 = end2.Y - (dk2 * end2.X);

                        if (dk1 != dk2 && dd1 != dd2)
                        {
                            double dsx = dsx = (dd2 - dd1) / (dk1 - dk2);
                            double dsy = (dk1 * dd2 - dk2 * dd1) / (dk1 - dk2);

                            g.DrawEllipse(pnow, (Int32)dsx, (Int32)dsy, 1, 1);
                            g.DrawString((Int32)dsx + "/" + (Int32)dsy, f1, Brushes.White, new Point((Int32)dsx + 5, (Int32)dsy - 8));
                        }
                    }
                }

        }
    }
}
