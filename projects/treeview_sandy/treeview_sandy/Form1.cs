﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace treeview_sandy
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			//add
			Form2 f = new Form2();
			f.ShowDialog();
			while (f.textBox1.Text.Length == 0 || f.textBox2.Text.Length == 0 || f.textBox3.Text.Length == 0)
			{
				MessageBox.Show("Nicht alles Ausgefüllt");
				f.ShowDialog();
			}
			TreeNode kunde = new TreeNode(f.textBox1.Text);
			kunde.Nodes.Add(f.textBox2.Text);
			kunde.Nodes.Add(f.textBox3.Text);
			treeView1.Nodes.Add(kunde);
		}

		private void button2_Click(object sender, EventArgs e)
		{
			//change
			if (treeView1.SelectedNode == null)
				return;
			Form2 f = new Form2();
			f.textBox1.Text = treeView1.SelectedNode.Text;
			f.textBox2.Text = treeView1.SelectedNode.Nodes[0].Text;
			f.textBox3.Text = treeView1.SelectedNode.Nodes[1].Text;
			f.ShowDialog();
			treeView1.SelectedNode.Text = f.textBox1.Text;
			treeView1.SelectedNode.Nodes[0].Text = f.textBox2.Text;
			treeView1.SelectedNode.Nodes[1].Text = f.textBox3.Text;
		}

		private void button3_Click(object sender, EventArgs e)
		{
			//remove
			if (treeView1.SelectedNode == null)
				return;
			DialogResult dr = MessageBox.Show("Wollen sie diese Node wirklich löschen?", "Wirklich?", MessageBoxButtons.YesNo);
			if (dr == DialogResult.Yes)
			{
				TreeNode t = treeView1.SelectedNode;
				while (t.Level > 0)
				{
					t = t.Parent;
				}
				treeView1.Nodes.Remove(t);
			}
		}

		private void button4_Click(object sender, EventArgs e)
		{
			string s = "";
			foreach (TreeNode t in treeView1.Nodes)
			{
				s += t.FullPath + "\n" + t.Nodes[0].FullPath + "\n" + t.Nodes[1].FullPath + "\n";
			}
			File.WriteAllText("output.txt", s);
		}

		private void button5_Click(object sender, EventArgs e)
		{
			if (!File.Exists("output.txt"))
				return;

			string[] lines = File.ReadAllLines("output.txt");
			for(int i = 0; i < lines.Length; i += 3)
			{
				TreeNode t = new TreeNode(lines[i]);
				t.Nodes.Add(lines[i + 1].Split('\\').Last());
				t.Nodes.Add(lines[i + 2].Split('\\').Last());
				treeView1.Nodes.Add(t);
			}
		}
	}
}
