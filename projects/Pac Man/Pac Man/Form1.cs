﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace Pac_Man
{
    public partial class Form1 : Form
    {
        PacMan _pacman;
        readonly playground _field = new playground(500, 500, 25);
        private int _points;
        public Form1()
        {
            InitializeComponent();
            ClientSize = _field.Size;
            Timer t = new Timer { Interval = 40 };
            t.Tick += t_Tick;
            t.Start();
            Timer frames = new Timer { Interval = 100 };
            frames.Tick += frames_Tick;
            SetupGamefield();
            DoubleBuffered = true;
            KeyDown += Form1_KeyDown;
            KeyUp += Form1_KeyUp;
            _pacman.TurnRight();
            _pacman.EatPill += pacman_EatPill;
            Text = @"0";
        }

        void pacman_EatPill(Pills p)
        {
            Debug.Assert(_field != null, "_field != null");
            _field.RemovePill(p);
            Debug.Assert(p != null, "p != null");
            _points += p.Value;
            Text = _points + "";
        }

        void Form1_KeyUp(object sender, KeyEventArgs e)
        {

            _pacman.RequestedChange = Direction.None;
        }

        void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    _pacman.RequestedChange = Direction.Up;
                    break;
                case Keys.Right:
                    _pacman.RequestedChange = Direction.Right;
                    break;
                case Keys.Down:
                    _pacman.RequestedChange = Direction.Down;
                    break;
                case Keys.Left:
                    _pacman.RequestedChange = Direction.Left;
                    break;

            }
        }

        void SetupGamefield()
        {
            Debug.Assert(_field != null, "_field != null");
            _field.Size = ClientRectangle.Size;
            _field.Location = new Point(0, 0);
            _field.BackColor = Color.LightBlue;
            _pacman = new PacMan(0, 0, 25, 25, _field.Width, _field.Height, 2, _field);
            _field.Controls.Add(_pacman);
            Controls.Add(_field);
        }

        void frames_Tick(object sender, EventArgs e)
        {
            Invalidate(true);
        }

        void t_Tick(object sender, EventArgs e)
        {
            if (Math.Abs((int)_pacman.CurrentDir - (int)_pacman.RequestedChange) == 2 ||
                (_pacman.Location.X % (Config.size) < Config.tolerance &&
                _pacman.Location.Y % (Config.size) < Config.tolerance))
            {
                switch (_pacman.RequestedChange)
                {
                    case Direction.Up:
                        _pacman.TurnUp();
                        break;
                    case Direction.Right:
                        _pacman.TurnRight();
                        break;
                    case Direction.Down:
                        _pacman.TurnDown();
                        break;
                    case Direction.Left:
                        _pacman.TurnLeft();
                        break;
                }
            }
            _pacman.DoStep();
            _pacman.Invalidate();
        }

    }
}
