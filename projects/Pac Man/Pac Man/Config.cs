﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    class Config
    {
        public static int size = 25;
        public static int tolerance = 10;
        public static int pill_tolerance = 2;
        public static int pill_size = 5;
    }
}
