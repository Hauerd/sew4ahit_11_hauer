﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Pac_Man
{
    class Pills : PictureBox
    {
        public int Value;

        public Pills(int x, int y)
        {
            Location = new Point(x, y);
            Width = Config.pill_size;
            Height = Config.pill_size;
            BackColor = Color.Red;
            Value = 1;
        }
    }
}
