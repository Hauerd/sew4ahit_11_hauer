﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Pac_Man
{
    class playground : MyPanel
    {
        public readonly List<Pills> Pills = new List<Pills>();
        public playground(int width, int height, int cellsize)
        {
            Size = new Size(width, height);
            Config.size = cellsize;
            Paint += playgroud_Paint;
            FillPills();
        }

        void playgroud_Paint(object sender, PaintEventArgs e)
        {
            //DrawDebugGrid(e.Graphics);
        }
        public void RemovePill(Pills p)

        {
 
            Controls.Remove(p);
            Pills.Remove(p);
        }

        readonly Random _rand = new Random();
        void FillPills()
        {
            int offset = Config.size / 2 - Config.pill_size / 2;
            for (int i = 0; i < Width; i += Config.size)
            {
                for (int j = 0; j < Height; j += Config.size)
                {
                    if (_rand.Next(0, 100) < 50)
                        Pills.Add(new Pills(i + offset, j + offset));
                    else
                        Pills.Add(new SpecialPills(i + offset, j + offset));
                }
            }
            Controls.AddRange(Pills.ToArray());
        }
        void UpdatePills()
        {
            foreach (Pills p in Pills)
            {
                p.Invalidate();
            }
        }

        void DrawDebugGrid(Graphics g)
        {
            int offset = Config.size / 2;
            for (int i = 0; i < Width / Config.size + 1; i++)
            {
                g.DrawLine(Pens.Black, i * Config.size + offset, 0, i * Config.size + offset, Height);
            }
            for (int i = 0; i < Height / Config.size; i++)
            {
                g.DrawLine(Pens.Black, 0, i * Config.size + offset, Width, i * Config.size + offset);
            }
        }
    }
}
