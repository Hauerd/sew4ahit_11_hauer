﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace Pac_Man
{
    enum Direction
    {
        None = 0,
        Up = 1,
        Right = 2,
        Down = 3,
        Left = 4
    }
    class PacMan : PictureBox
    {

        public delegate void EatPillHandler(Pills p);
        public event EatPillHandler EatPill;

        readonly playground _gameField;

        double _startAngle;
        double _sweepAngle = 360;
        int _counter;

        private readonly int _stepWidth;

        public Direction CurrentDir = Direction.None;
        public Direction RequestedChange = Direction.None;

        public PacMan(int x, int y, int width, int height, int maxWidth, int maxHeight, int stepWidth, playground p)
        {
            Left = x;
            Top = y;
            Width = width;
            Height = height;
            _stepWidth = stepWidth;
            _gameField = p;

            Paint += PacMan_Paint;
        }

        public void TurnUp()
        {
            CorrectPosition();
            CurrentDir = Direction.Up;
        }
        public void TurnDown()
        {
            CorrectPosition();
            CurrentDir = Direction.Down;
        }
        public void TurnLeft()
        {
            CorrectPosition();
            CurrentDir = Direction.Left;
        }
        public void TurnRight()
        {
            CorrectPosition();
            CurrentDir = Direction.Right;
        }

        private void CorrectPosition()
        {
            if (RequestedChange == Direction.None || RequestedChange == CurrentDir) return;

            if (Location.X % Config.size < Config.tolerance)
                Left -= Location.X % Config.size;
            else
                Left += Config.size - Location.X % Config.size;

            if (Location.Y % Config.size < Config.tolerance)
                Top -= Location.Y % Config.size;
            else
                Top += Config.size - Location.Y % Config.size;
        }

        private void CheckForPill()
        {
            Pills pill = _gameField.Pills.Find(p =>
            {
                Rectangle r1 = new Rectangle(Left, Top, Width, Height);
                Rectangle r2 = new Rectangle(p.Left, p.Top, p.Width, p.Height);
                return r1.IntersectsWith(r2);
            });

            if (pill == null) return;

            Debug.Assert(EatPill != null, "EatPill != null");
            EatPill(pill);
        }

        public void DoStep()
        {
            _counter++;
            _startAngle = Math.Abs(Math.Sin(_counter) * 50);
            _sweepAngle = Math.Abs(360 - Math.Sin(_counter) * 100);
            switch (CurrentDir)
            {
                case Direction.Right:
                    if (Left + Width + _stepWidth < _gameField.Width)
                        Left += _stepWidth;
                    break;
                case Direction.Down:
                    _startAngle += 90;
                    if (Top + Height + _stepWidth < _gameField.Height)
                        Top += _stepWidth;
                    break;
                case Direction.Left:
                    _startAngle += 180;
                    if (Left - _stepWidth > 0)
                        Left -= _stepWidth;
                    break;
                case Direction.Up:
                    _startAngle += 270;
                    if (Top - _stepWidth > 0)
                        Top -= _stepWidth;
                    break;
            }
            CheckForPill();
        }

        void PacMan_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.FillPie(new SolidBrush(Color.FromArgb(255, 255, 238, 00)), new Rectangle(0, 0, Width, Height), (float)_startAngle, (float)_sweepAngle);
        }
    }
}
