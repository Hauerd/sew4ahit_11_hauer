﻿using System.Drawing;

namespace Pac_Man
{
    class SpecialPills : Pills
    {
        public SpecialPills(int x, int y) : base(x, y) {

            Location = new Point(x, y);
            Width = Config.pill_size;
            Height = Config.pill_size;
            BackColor = Color.Blue;
            Value = 5;
        }
    }
}
