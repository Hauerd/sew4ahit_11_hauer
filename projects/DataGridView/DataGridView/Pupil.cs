﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGridView
{
    public class Pupil
    {
        public Pupil(int index, int shoesize, string lastname, string firstname)
        {
            Index = index;
            Shoesize = shoesize;
            Lastname = lastname;
            Firstname = firstname;
        }

        public int Index, Shoesize;
        public string Lastname, Firstname;
    }
}
