﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DataGridView
{
    public partial class Form1 : Form
    {
        private readonly List<Pupil> pupils = new List<Pupil>();
        private int selectedRow = 0;
        public Form1()
        {
            InitializeComponent();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridView1.ColumnCount = 4;
            dataGridView1.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns[0].Name = "Schülernummer";
            dataGridView1.Columns[1].Name = "Nachname";
            dataGridView1.Columns[2].Name = "Vorname";
            dataGridView1.Columns[3].Name = "Schuhgröße";

            dataGridView1.CellMouseDown += dataGridView1_CellMouseDown;
            LoadFile("pupils.json");
            LoadToDataSet();
            dataGridView1.AutoSize = true;

        }
        private void deleteRowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedRow == 0 && dataGridView1.Rows.Count == 0) return;

            dataGridView1.Rows.RemoveAt(selectedRow);
            pupils.RemoveAt(selectedRow);
            SaveToFile("pupils.json");
            selectedRow = 0;
            dataGridView1.ClearSelection();
        }

        void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridView1.ClearSelection();
            if (e.Button == MouseButtons.Right)
            {
                dataGridView1.Rows[e.RowIndex].Selected = true;
                selectedRow = e.RowIndex;
            }
        }

        private void LoadToDataSet()
        {
            pupils.ForEach(pupil =>
            {
                dataGridView1.Rows.Add(pupil.Index, pupil.Lastname, pupil.Firstname, pupil.Shoesize);
            });
        }

        private void SaveDemoPupils()
        {
            pupils.Clear();
            pupils.Add(new Pupil(1, 42, "Huaba", "Koal"));
            pupils.Add(new Pupil(10, 700, "0.0", "Josef"));
            pupils.Add(new Pupil(14, 46, "Herbert", "Bittermann"));
            pupils.Add(new Pupil(21, 1, "PANH", "Peter"));
            SaveToFile("pupils.json");
        }

        private void LoadFile(string file)
        {
            if (!File.Exists(file)) throw new FileNotFoundException();

            object o = JsonConvert.DeserializeObject(File.ReadAllText(file));
            if (o.GetType() == typeof(JArray))
            {
                pupils.Clear();
                foreach (var s in (JArray)o)
                    pupils.Add(s.ToObject<Pupil>());
            }
            else
            {
                throw new ArgumentException("the file is invalid");
            }
        }

        private void SaveToFile(string file)
        {
            File.WriteAllText(file, JsonConvert.SerializeObject(pupils));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveToFile("pupils.json");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveDemoPupils();
            LoadFile("pupils.json");
            LoadToDataSet();
        }

    }
}
