﻿// ---------------------------------------------------------------
// Klasse: 	Programm
// Zweck: 	Request händeln bzw zurücksenden 
// Projekt: 	WebServer
// Autor: 	Dominik Hauer
// Datum: 	06.05.2015
// Version: 	1.0
// -----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Threading;
using System.IO;

namespace TestServer
{

    class Program
    {
        static TcpListener listener;
        const int LIMIT = 1; //Limit für Threads setzten
        static string[] parts;
        static string ROOTDIRECTORY = Directory.GetCurrentDirectory() + "\\htdocs";        //RootDirectory für den Webbrowser festlegen
        static StreamWriter sw;

        static void Main(string[] args)
        {
           

            listener = new TcpListener(IPAddress.Any,8888);
            listener.Start();

            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }

        //Dient als Service Methode wo Empfangen und Gesendet wird
        public static void Service()
        { 
            
                   while(true){
            
            Socket soc = listener.AcceptSocket();

            #if LOG
                Console.WriteLine("Connected: {0}", 
                                         soc.RemoteEndPoint);
            #endif

            try{

                
                Stream s = new NetworkStream(soc);
                StreamReader sr = new StreamReader(s);
                sw = new StreamWriter(s);
                sw.AutoFlush = true;
                HttpRequest request = Receive(sr);
                foreach(byte b in request.Response)
                {
                    s.WriteByte(b);
                }

                s.Flush();
                s.Close();

            }catch(Exception){
                #if LOG
                    Console.WriteLine(e.Message);
                #endif
            }
            #if LOG
                Console.WriteLine("Disconnected: {0}", 
                                        soc.RemoteEndPoint);
            #endif
            soc.Close();
        
        }
    }

        //Recieves the Request from the StreamReader
        public static HttpRequest Receive(StreamReader sr)
        {
            HttpRequest hp = new HttpRequest();
            parts = sr.ReadLine().Split(' ');
            hp.Methodtype = parts[0];
            hp.Pfad = parts[1].Replace('/', '\\');
            hp.Httpversion =  parts[2];
             
            while (true)
            {
                string head = sr.ReadLine();
                if (head.Length <= 1)
                    break;
                
                string[]headers;

                headers = head.Replace(" ", "").Replace("\t", "").Split(':');
                hp.Headers.Add(headers[0], headers[1]);
            }

            //überprüft bis keine Zeile mehr zum Schreiben da ist 
            while (sr.Peek() != -1)
                hp.Body += sr.ReadLine() + "\r\n";

            CreateResponse(hp);
            return hp;
        }

        //Generiert den Einkommenden Request 
        public static void CreateResponse(HttpRequest hp)
        {
                List<byte> response = null;
                string request = "";
                bool img = false;

               if (hp.Methodtype == "GET")
                {
                   //Überprüfuen ob das File exestiert 
                    if (File.Exists(ROOTDIRECTORY +hp.Pfad))
                    {
                        request += "HTTP/1.1 200 OK\r\n";
                        if (hp.Pfad.EndsWith(".png") | hp.Pfad.EndsWith(".jpg") | hp.Pfad.EndsWith(".jpg"))
                        {
                            
                            List<byte> picture = File.ReadAllBytes(ROOTDIRECTORY + hp.Pfad).ToList();
                            
                            if(hp.Pfad.EndsWith(".png"))
                                request += "Content-Type : image/png\r\n";
                            if(hp.Pfad.EndsWith(".jpg"))
                                request += "Content-Type : image/jpg\r\n";
                            if(hp.Pfad.EndsWith(".gif"))
                                request += "Content-Type : image/gif\r\n";
                            request += "Content-Length : " + picture.Count + "\r\n\r\n";
                            img = true;
                            response = Encoding.UTF8.GetBytes(request).ToList();
                            response.AddRange(picture);
                            

                        }
                        else
                        {
                            request += "\r\n\r\n" + File.ReadAllText(ROOTDIRECTORY + hp.Pfad);
                        }
                    }
                    //Überprüfuen ob das Ordner exestiert 
                    else if (Directory.Exists(ROOTDIRECTORY+hp.Pfad))
                    {
                        request += "HTTP/1.1 200 OK\r\n\r\n";
                        request += File.ReadAllText(ROOTDIRECTORY + hp.Pfad.TrimEnd('\\') + "\\index.html");
                    }
                    else
                    {
                        request += "HTTP/1.1 404 Not Found\r\n\r\n";
                        request += File.ReadAllText(ROOTDIRECTORY + "\\FileNotFound.html");
                    }
                }
                else
                {
                    request += "HTTP/1.1 403 Forbitten\r\n\r\n";
                    request += File.ReadAllText(ROOTDIRECTORY + "\\FileNotFound.html");
                }

               if (img == false)
               {
                   response = Encoding.UTF8.GetBytes(request).ToList();
                   img = false;
               }
               
                   hp.Response = response;
               
               
        }
}}
