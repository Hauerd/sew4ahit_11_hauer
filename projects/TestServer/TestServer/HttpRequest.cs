﻿// ---------------------------------------------------------------
// Klasse: 	HttpRequest
// Zweck: 	Request händeln bzw zurücksenden 
// Projekt: 	WebServer
// Autor: 	Dominik Hauer
// Datum: 	06.05.2015
// Version: 	1.0
// -----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    class HttpRequest
    {
        public string Methodtype {get ; set;}
        public string Pfad {get ; set;}
        public string Httpversion{get ; set;}
        public Dictionary<string, string> Headers{get ; set;}
        public string Body{get ; set;}
        public List<byte> Response{get;set;}

        public HttpRequest()
        {
            Headers = new Dictionary<string, string>();
        }
    }
}
