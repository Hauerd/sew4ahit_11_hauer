﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace DataGrigView_DataBase
{
    public partial class Form1 : Form
    {
        OpenFileDialog of;
        OleDbConnection con;
        DataSet ds = new DataSet();
        
        public Form1()
        {
            InitializeComponent();
            button2.Hide();
            dataGridView1.AutoResizeColumns();
            dataGridView1.AutoResizeRows();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            of = new OpenFileDialog();
            of.Filter = "Access-Datenbanken | *.accdb;*.mdb";
            DialogResult dr = of.ShowDialog();
            
            if (dr == DialogResult.OK)
            {
                con = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;data Source=" + of.FileName);
                con.Open();
                DataTable dataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, null });

                for (int i = 0; i < dataTable.Rows.Count; i++)
                    if (dataTable.Rows[i]["TABLE_TYPE"].ToString() == "TABLE")
                        listBox1.Items.Add(dataTable.Rows[i]["TABLE_NAME"].ToString());
                button2.Show();
                listBox1.SelectedIndex = 1;
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string tabelle = listBox1.SelectedItem.ToString();
            OleDbDataAdapter adapter = new OleDbDataAdapter("Select * from " + tabelle,con);
            adapter.Fill(ds, tabelle);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = tabelle;
        }
    }
}
