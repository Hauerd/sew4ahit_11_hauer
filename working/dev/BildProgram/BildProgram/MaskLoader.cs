﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace BildProgram
{
    class MaskLoader
    {
        static public List<Mask> LoadConfig()
        {
            XmlReader xmlReader = XmlReader.Create("mask.xml");
            List<Mask> mask = new List<Mask>();
            while (xmlReader.Read())
            {
                if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "mask"))
                {
                    if (xmlReader.HasAttributes)
                    {
                        mask.Add(new Mask(xmlReader.GetAttribute("name"),Convert.ToInt32(xmlReader.GetAttribute("red")),Convert.ToInt32(xmlReader.GetAttribute("green")),Convert.ToInt32(xmlReader.GetAttribute("blue"))));
                    }
                }
            }
            
            xmlReader.Close();
            return mask;
        }


        static public void SaveConfig(List<Mask> m)
        {
            XmlTextWriter myXmlTextWriter = new XmlTextWriter("mask.xml", null);
            myXmlTextWriter.Formatting = Formatting.Indented;
            myXmlTextWriter.WriteStartDocument(false);

            myXmlTextWriter.WriteStartElement("maprj");
            myXmlTextWriter.WriteStartElement("data", null);
            foreach (Mask mask in m)
            {
                myXmlTextWriter.WriteStartElement("mask", null);
                myXmlTextWriter.WriteAttributeString("name", mask.name);
                myXmlTextWriter.WriteAttributeString("red", Convert.ToString(mask.redfaktor));
                myXmlTextWriter.WriteAttributeString("green", Convert.ToString(mask.greenfaktor));
                myXmlTextWriter.WriteAttributeString("blue", Convert.ToString(mask.bluefaktor));
                myXmlTextWriter.WriteEndElement();
            }
            myXmlTextWriter.WriteEndElement();
            myXmlTextWriter.WriteEndElement();

            myXmlTextWriter.Flush();
            myXmlTextWriter.Close();

            Console.ReadLine();


        }
    }

}