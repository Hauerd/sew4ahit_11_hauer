﻿namespace BildProgram
{
    partial class MaskView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaskView));
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.FotoUpdate = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.MaskListBox = new System.Windows.Forms.ListBox();
            this.MaskTextBox = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.MaskDelete = new System.Windows.Forms.PictureBox();
            this.AddMask = new System.Windows.Forms.PictureBox();
            this.RedTextBox = new System.Windows.Forms.TextBox();
            this.GreenTextBox = new System.Windows.Forms.TextBox();
            this.BlueTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.RedMaskBar = new System.Windows.Forms.HScrollBar();
            this.GreenMaskBar = new System.Windows.Forms.HScrollBar();
            this.BlueMaskBar = new System.Windows.Forms.HScrollBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ColorPanel = new System.Windows.Forms.Panel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaskDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddMask)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox.Location = new System.Drawing.Point(1, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(745, 417);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // FotoUpdate
            // 
            this.FotoUpdate.Location = new System.Drawing.Point(69, 481);
            this.FotoUpdate.Name = "FotoUpdate";
            this.FotoUpdate.Size = new System.Drawing.Size(250, 44);
            this.FotoUpdate.TabIndex = 1;
            this.FotoUpdate.Text = "Foto Update";
            this.FotoUpdate.UseVisualStyleBackColor = true;
            this.FotoUpdate.Click += new System.EventHandler(this.FotoUpdate_Click);
            // 
            // Save
            // 
            this.Save.Enabled = false;
            this.Save.Location = new System.Drawing.Point(408, 481);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(315, 43);
            this.Save.TabIndex = 2;
            this.Save.Text = "Foto Speichern";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // MaskListBox
            // 
            this.MaskListBox.Enabled = false;
            this.MaskListBox.FormattingEnabled = true;
            this.MaskListBox.Location = new System.Drawing.Point(51, 13);
            this.MaskListBox.Name = "MaskListBox";
            this.MaskListBox.ScrollAlwaysVisible = true;
            this.MaskListBox.Size = new System.Drawing.Size(176, 43);
            this.MaskListBox.TabIndex = 3;
            this.MaskListBox.SelectedIndexChanged += new System.EventHandler(this.MaskListBox_SelectedIndexChanged);
            // 
            // MaskTextBox
            // 
            this.MaskTextBox.Location = new System.Drawing.Point(59, 58);
            this.MaskTextBox.Name = "MaskTextBox";
            this.MaskTextBox.Size = new System.Drawing.Size(168, 20);
            this.MaskTextBox.TabIndex = 4;
            this.MaskTextBox.Text = "MaskName";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(806, 69);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(43, 47);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // MaskDelete
            // 
            this.MaskDelete.Enabled = false;
            this.MaskDelete.Image = ((System.Drawing.Image)(resources.GetObject("MaskDelete.Image")));
            this.MaskDelete.Location = new System.Drawing.Point(73, 94);
            this.MaskDelete.Name = "MaskDelete";
            this.MaskDelete.Size = new System.Drawing.Size(42, 45);
            this.MaskDelete.TabIndex = 7;
            this.MaskDelete.TabStop = false;
            this.MaskDelete.Click += new System.EventHandler(this.MaskDelete_Click);
            // 
            // AddMask
            // 
            this.AddMask.Enabled = false;
            this.AddMask.Image = ((System.Drawing.Image)(resources.GetObject("AddMask.Image")));
            this.AddMask.Location = new System.Drawing.Point(153, 94);
            this.AddMask.Name = "AddMask";
            this.AddMask.Size = new System.Drawing.Size(42, 45);
            this.AddMask.TabIndex = 9;
            this.AddMask.TabStop = false;
            this.AddMask.Click += new System.EventHandler(this.AddMask_Click);
            // 
            // RedTextBox
            // 
            this.RedTextBox.Enabled = false;
            this.RedTextBox.Location = new System.Drawing.Point(199, 185);
            this.RedTextBox.Name = "RedTextBox";
            this.RedTextBox.Size = new System.Drawing.Size(28, 20);
            this.RedTextBox.TabIndex = 10;
            this.RedTextBox.Text = "0";
            this.RedTextBox.TextChanged += new System.EventHandler(this.RedTextBox_TextChanged);
            // 
            // GreenTextBox
            // 
            this.GreenTextBox.Enabled = false;
            this.GreenTextBox.Location = new System.Drawing.Point(199, 242);
            this.GreenTextBox.Name = "GreenTextBox";
            this.GreenTextBox.Size = new System.Drawing.Size(28, 20);
            this.GreenTextBox.TabIndex = 11;
            this.GreenTextBox.Text = "0";
            this.GreenTextBox.TextChanged += new System.EventHandler(this.GreenTextBox_TextChanged);
            // 
            // BlueTextBox
            // 
            this.BlueTextBox.Enabled = false;
            this.BlueTextBox.Location = new System.Drawing.Point(199, 302);
            this.BlueTextBox.Name = "BlueTextBox";
            this.BlueTextBox.Size = new System.Drawing.Size(28, 20);
            this.BlueTextBox.TabIndex = 12;
            this.BlueTextBox.Text = "0";
            this.BlueTextBox.TextChanged += new System.EventHandler(this.BlueTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Rot";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 229);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Grün";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 290);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Blau";
            // 
            // RedMaskBar
            // 
            this.RedMaskBar.Enabled = false;
            this.RedMaskBar.Location = new System.Drawing.Point(51, 188);
            this.RedMaskBar.Maximum = 109;
            this.RedMaskBar.Minimum = -100;
            this.RedMaskBar.Name = "RedMaskBar";
            this.RedMaskBar.Size = new System.Drawing.Size(120, 17);
            this.RedMaskBar.TabIndex = 16;
            this.RedMaskBar.ValueChanged += new System.EventHandler(this.RedMaskBar_ValueChanged);
            // 
            // GreenMaskBar
            // 
            this.GreenMaskBar.Enabled = false;
            this.GreenMaskBar.Location = new System.Drawing.Point(51, 260);
            this.GreenMaskBar.Maximum = 109;
            this.GreenMaskBar.Minimum = -100;
            this.GreenMaskBar.Name = "GreenMaskBar";
            this.GreenMaskBar.Size = new System.Drawing.Size(120, 17);
            this.GreenMaskBar.TabIndex = 17;
            this.GreenMaskBar.ValueChanged += new System.EventHandler(this.GreenMaskBar_ValueChanged);
            // 
            // BlueMaskBar
            // 
            this.BlueMaskBar.Enabled = false;
            this.BlueMaskBar.Location = new System.Drawing.Point(54, 317);
            this.BlueMaskBar.Maximum = 109;
            this.BlueMaskBar.Minimum = -100;
            this.BlueMaskBar.Name = "BlueMaskBar";
            this.BlueMaskBar.Size = new System.Drawing.Size(120, 17);
            this.BlueMaskBar.TabIndex = 18;
            this.BlueMaskBar.ValueChanged += new System.EventHandler(this.BlueMaskBar_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BlueMaskBar);
            this.panel1.Controls.Add(this.GreenMaskBar);
            this.panel1.Controls.Add(this.RedMaskBar);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.BlueTextBox);
            this.panel1.Controls.Add(this.GreenTextBox);
            this.panel1.Controls.Add(this.RedTextBox);
            this.panel1.Controls.Add(this.AddMask);
            this.panel1.Controls.Add(this.MaskDelete);
            this.panel1.Controls.Add(this.MaskTextBox);
            this.panel1.Controls.Add(this.MaskListBox);
            this.panel1.Location = new System.Drawing.Point(752, 192);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(289, 387);
            this.panel1.TabIndex = 19;
            // 
            // ColorPanel
            // 
            this.ColorPanel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ColorPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ColorPanel.Location = new System.Drawing.Point(900, 69);
            this.ColorPanel.Name = "ColorPanel";
            this.ColorPanel.Size = new System.Drawing.Size(41, 47);
            this.ColorPanel.TabIndex = 20;
            this.ColorPanel.Click += new System.EventHandler(this.ColorPanel_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::BildProgram.Properties.Resources.close_micro;
            this.pictureBox2.Location = new System.Drawing.Point(987, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 28);
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // MaskView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 578);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.ColorPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.FotoUpdate);
            this.Controls.Add(this.pictureBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MaskView";
            this.Text = "MaskView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MaskView_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaskDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddMask)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button FotoUpdate;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.ListBox MaskListBox;
        private System.Windows.Forms.TextBox MaskTextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox MaskDelete;
        private System.Windows.Forms.PictureBox AddMask;
        private System.Windows.Forms.TextBox RedTextBox;
        private System.Windows.Forms.TextBox GreenTextBox;
        private System.Windows.Forms.TextBox BlueTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.HScrollBar RedMaskBar;
        private System.Windows.Forms.HScrollBar GreenMaskBar;
        private System.Windows.Forms.HScrollBar BlueMaskBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel ColorPanel;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}