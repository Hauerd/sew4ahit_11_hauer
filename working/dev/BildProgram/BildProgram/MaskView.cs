﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace BildProgram
{
    public partial class MaskView : Form
    {
        List<string> s = new List<string>();
        List<Mask> m = new List<Mask>();
        Dictionary<Color,List<Point>> c_Points;
        List<PenPoint> full_Points;

        Mask miwas;
        string phad;
        string name;
        Image img;
        Bitmap bmpold;
        bool m_Drawing;
        int i = 3;
        bool isImage;


        public MaskView()
        {
            InitializeComponent();
            GetMask();
            c_Points = new Dictionary<Color, List<Point>>();
            full_Points = new List<PenPoint>();
            this.MouseWheel += new MouseEventHandler(pictureBox_MouseWheel);

        }

        public void GetMask() 
        {
            m = MaskLoader.LoadConfig();

            foreach (var mask in m)
            {
                s.Add(mask.name);
            }

            foreach (var name in s)
            {

                MaskListBox.Items.Add(name);
            }

        }

        void SelectMask() { }
        void DeleteMask() { }
        void UpdateMask() { }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void ColorPanel_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            ColorPanel.BackColor = colorDialog1.Color;
        }

        private void FotoUpdate_Click(object sender, EventArgs e)
        {

            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Images|*.png";
            fd.ShowDialog();
            name = fd.SafeFileName;
            phad = fd.FileName;

            img = Image.FromFile(phad);
            pictureBox.Image = img;
            bmpold = new Bitmap(pictureBox.Image);

            #region Enable
            isImage = true;
            BlueMaskBar.Enabled = true;
            BlueTextBox.Enabled = true;
            RedMaskBar.Enabled = true;
            RedTextBox.Enabled = true;
            GreenMaskBar.Enabled = true;
            GreenTextBox.Enabled = true;
            AddMask.Enabled = true;
            MaskDelete.Enabled = true;
            MaskListBox.Enabled = true;
            Save.Enabled = true;
            #endregion
        }

        private void Save_Click(object sender, EventArgs e)
        {
            string direct = @"c:\\";
            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = "Images|*.png";
            sf.FileName = name;
            sf.InitialDirectory = direct;
            Bitmap bmp = new Bitmap(pictureBox.Width, pictureBox.Height);
            pictureBox.DrawToBitmap(bmp, new Rectangle(0, 0, pictureBox.Width, pictureBox.Height));
            

            if (sf.ShowDialog() == DialogResult.OK)
            {
                direct = sf.FileName;
                bmp.Save(direct, ImageFormat.Png);
            }
            else
            {

            }
        }
        #region Blau
        private void BlueMaskBar_ValueChanged(object sender, EventArgs e)
        {
            int i = BlueMaskBar.Value;
            BlueTextBox.Text = Convert.ToString(i);
        }

        private void BlueTextBox_TextChanged(object sender, EventArgs e)
        {
            String text = BlueTextBox.Text;
            if (text != "-" && text != "")
            {
                int i = int.Parse(text);
                BlueMaskBar.Value = i;

                Photochange();
            }
        }
        #endregion
        #region Rot
        private void RedMaskBar_ValueChanged(object sender, EventArgs e)
        {
            int i = RedMaskBar.Value;
            RedTextBox.Text = Convert.ToString(i);
        }

        private void RedTextBox_TextChanged(object sender, EventArgs e)
        {
            String text = RedTextBox.Text;
            if (text != "-" && text != "")
            {
                int i = int.Parse(text);
                RedMaskBar.Value = i;

                Photochange();
            }
        }
        #endregion
        #region Grün
        private void GreenMaskBar_ValueChanged(object sender, EventArgs e)
        {
            int i = GreenMaskBar.Value;
            GreenTextBox.Text = Convert.ToString(i);
        }

        private void GreenTextBox_TextChanged(object sender, EventArgs e)
        {
            String greentext = GreenTextBox.Text;

            if (greentext != "-" && greentext != "")
            {
                int g = int.Parse(greentext);
                GreenMaskBar.Value = g;

                Photochange();
            }
        }
        #endregion

        private void Photochange()
        {
                int g = int.Parse(GreenTextBox.Text);
                int b = int.Parse(BlueTextBox.Text);
                int r = int.Parse(RedTextBox.Text);
                Color c ;
                int blue ;
                int red ;
                int green;
                Bitmap bmp = new Bitmap(pictureBox.Image);

                for (int y = 0; y < bmpold.Width; y++)
                {
                    for (int z = 0; z < bmpold.Height; z++)
                    {

                        c = bmpold.GetPixel(y, z);
                        red = ((int)(c.R * ((double)(100 + r) / 100)));
                        blue = ((int)(c.B * ((double)(100 + b) / 100)));
                        green = ((int)(c.G * ((double)(100 + g) / 100)));
                        if (green > 255)
                            green = 255;
                        if (blue > 255)
                            blue = 255;
                        if (red > 255)
                            red = 255;
                        
                        
                        bmp.SetPixel(y, z, Color.FromArgb(red, green, blue));
                    }
                }

                pictureBox.Image = Image.FromHbitmap(bmp.GetHbitmap());
                pictureBox.Invalidate();
            }

        private void AddMask_Click(object sender, EventArgs e)
        {
            m.Add(new Mask(MaskTextBox.Text, Convert.ToInt32(RedTextBox.Text), Convert.ToInt32(GreenTextBox.Text), Convert.ToInt32(BlueTextBox.Text)));

            s.Add(MaskTextBox.Text);

            MaskListBox.Items.Clear();
            foreach (var mask in s)
            {

                MaskListBox.Items.Add(mask); 
            }

            MaskListBox.SelectedItem = null;
        }

        private void MaskListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!(MaskListBox.SelectedItem == null))
            {
                string selecteditem = (string)MaskListBox.SelectedItem;

                Mask fm = m.Find((a) => { return a.name == selecteditem; });

                MaskTextBox.Text = fm.name;
                RedTextBox.Text = Convert.ToString(fm.redfaktor);
                BlueTextBox.Text = Convert.ToString(fm.bluefaktor);
                GreenTextBox.Text = Convert.ToString(fm.greenfaktor);
            }
        }

        private void MaskDelete_Click(object sender, EventArgs e)
        {
                string selecteditem = (string)MaskListBox.SelectedItem;
                Mask fm = m.Find((a) => { return a.name == selecteditem; });


                MaskListBox.Items.Remove(fm.name);
                m.Remove(fm);
                s.Remove(fm.name);
                MaskListBox.SelectedItem = null;

                MaskTextBox.Text = "new";
                RedTextBox.Text = Convert.ToString(0);
                BlueTextBox.Text = Convert.ToString(0);
                GreenTextBox.Text = Convert.ToString(0);
        }

        private void MaskView_FormClosing(object sender, FormClosingEventArgs e)
        {
            MaskLoader.SaveConfig(m);
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            full_Points.Add(new PenPoint(i, new List<Point>(), ColorPanel.BackColor));
            m_Drawing = true;
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            m_Drawing = false;
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            foreach (var points in full_Points)
            {
                if (points.points.Count > 1)
                {
                    e.Graphics.DrawLines(new Pen(new SolidBrush(points.color), points.radius), points.points.ToArray());
                }
            }

        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_Drawing)
            {
                full_Points[full_Points.Count - 1].points.Add(e.Location);

                this.Refresh();
            }
        }

        private void pictureBox_MouseWheel(object sender, MouseEventArgs e)
        {
            

            if (e.Delta > 0)
            {
                if (i < 10)
                    i++;
                else
                    i = 10;
            }
            else
            {
                if(i>2)
                i--;
                else
                {
                    i = 1;
                }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
