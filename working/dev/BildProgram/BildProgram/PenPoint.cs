﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace BildProgram
{
     class PenPoint
    {

        public int radius;
        public List<Point> points;
       public Color color;
 
        public PenPoint(int radius, List<Point> points, Color color)
        {

            this.radius = radius;
            this.points = points;
            this.color = color;
        }
    }
}
