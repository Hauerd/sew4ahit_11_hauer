﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BildProgram
{
    class Mask
    {
        public string name;
        public int redfaktor;
        public int greenfaktor;
        public int bluefaktor;

        public Mask(string name, int r, int g, int b)
        {
            this.name = name;
            this.redfaktor = r;
            this.greenfaktor = g;
            this.bluefaktor = b;
        }
    }
}
