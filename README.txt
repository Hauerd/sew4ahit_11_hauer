Bildmanipulation Programm

Meine Projektidee besteht darin, ein Programm zu entwickeln welches ein Foto einliest, welches man dann bearbeiten bzw. mit einen Pinsel etwas hinzuzeichnen kann. 
Genau will ich das Bild Bit-weise einlesen und danach Regler im Programm hinzuf�gen, welche die Helligkeit der einzelnen Farben (Rot, Gelb, Blau) ver�ndern kann und dazu noch einen �Pinsel� implementiere, wo man eine Farbe ausw�hlen kann und damit das Bild �berzeichnen kann. 
Die durch die Regler neu erstellte �Maske� kann man  in einer ListBox ausw�hlen, welche in einer Datenbank gespeichert sind. Zum Schluss soll man dann das Bild wieder abspeichern k�nnen.
